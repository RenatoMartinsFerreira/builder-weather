# builder-weather

Aplicativo de Clima desenvolvido em 2 dias para pleitear vaga como desenvolvido React Native.

O prototipo da tela inicial pode ser acessado por https://www.figma.com/file/GZaY0K1vmcOTwFqnXMitSz/builders-weather?node-id=0%3A1

O projeto consome a previsão do tempo e endereço da API Open Weather Map: http://api.openweathermap.org/ 

Foram utilizadas as bibliotecas: "react-native-maps": 0.26.1, "redux": 4.0.5 e "redux-thunk": 2.3.0.

Para o gerenciamento de dados da aplicação foi utilizada a model Weather, onde é centralizada a comunicação com o Redux, Serviçoes e telas, como descrito em documentacao/arquitetura.pdf

