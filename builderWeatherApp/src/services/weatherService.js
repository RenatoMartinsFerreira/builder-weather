import React from 'react';

export class WeatherService extends React.Component {
  constructor(props) {
    super(props);
  }

  getWeatherInfo = (lat, lon) =>
    fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&APPID=b656df4059dcafaa150504e076528bf5`)
    // fetch(`https://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22`)
      .then((response) => response.json())
      .then((responseJson) => {
        return(responseJson)
      })
      .catch((error) => {
        return (error)
      });
}
export default WeatherService;
