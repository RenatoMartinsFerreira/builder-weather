import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  Dimensions,
} from 'react-native';
import Store from "builderWeatherApp/src/redux/store";
import colors from 'builderWeatherApp/src/commons/colors'
import { Footer } from 'builderWeatherApp/src/components/container'
import { GenericCircularText } from 'builderWeatherApp/src/components/presentation'

import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
export class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weather: 0,
    }
    Store.subscribe(() => this.setState({ weather: Store.getState().weatherReducer.finish }))
  }

  render() {
    return (
      <View style={styles.container} >
        <StatusBar backgroundColor={colors.gold} barStyle="dark-content" />

        <View style={styles.content} >

          <View style={styles.floatingInformation} >
            <View style={{ margin: 10 }} >
              <GenericCircularText
                text={'Chuva: ' + this.state.weather.clouds + '%'}
              />
            </View>
            <View style={{ margin: 10 }} >
              <GenericCircularText
                text={'Umidade: ' + this.state.weather.humidity + '%'}
              />
            </View>
            <View style={{ margin: 10 }} >
              <GenericCircularText
                text={'Vento: ' + this.state.weather.windSpeed + 'km/h'}
              />
            </View>
          </View>


          <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            region={{
              latitude: this.state.weather.lat ? this.state.weather.lat : 0 ,
              longitude: this.state.weather.lon ? this.state.weather.lon : 0 ,
              latitudeDelta: 0.015,
              longitudeDelta: 0.0121,
            }}
          >
            {!!this.state.weather.lat && !!this.state.weather.lon && <MapView.Marker
              coordinate={{"latitude":this.state.weather.lat,"longitude":this.state.weather.lon}}
            />}
          </MapView>


        </View>

        <View style={styles.footerContainer} >
          <Footer style={styles.footer} />
        </View>

      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 3,
    backgroundColor: colors.AntiFlashWhite
  },
  map:{
    flex: 1,
  },
  footer: {
    flex: 1,
  },
  footerContainer: {
    zIndex: 1,
    bottom: 0,
    position: 'absolute',
    height: Dimensions.get('window').height / 3,
    width: Dimensions.get('window').width
  },
  floatingInformation: {
    zIndex: 1,
    position: 'absolute',
    right: 0,
    top: Dimensions.get('window').height / 9,
  }
});
