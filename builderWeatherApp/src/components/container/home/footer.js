import React from 'react';
import { StyleSheet, View } from 'react-native';

import { GenericButton, GenericTextComponent, GenericWeatherComponent } from 'builderWeatherApp/src/components/presentation'
import colors from 'builderWeatherApp/src/commons/colors';
import { WeatherModel } from 'builderWeatherApp/src/model'

export class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            weather: 0,
        }
        weatherModel = new WeatherModel()
    }

    updateWeather = () => {
        weatherModel.getCurrentWeather().then((response) => {
            this.setState({ weather: response });
        })
    }

    componentDidMount() {
        this.updateWeather();
    }
    render() {
        return (
            <View style={styles.container} >
                <View style={styles.content} >
                    <View style={styles.addressContainer} >
                        <GenericTextComponent
                            text={this.state.weather.place}
                            textAlign={'center'} 
                        />
                    </View>
                    <View style={styles.weatherContainer} >
                        <GenericWeatherComponent
                            max={this.state.weather.temp_max}
                            min={this.state.weather.temp_min}
                            current={this.state.weather.temp}
                        />
                    </View>
                </View>

                <View style={styles.buttonContainer} >
                    <GenericButton
                        text={'Atualizar previsão'}
                        onPress={() => { this.updateWeather() }}
                    />
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
        borderColor: colors.gold,
        borderBottomColor: colors.white,
        borderWidth: 1,
        borderTopEndRadius: 14,
        borderTopStartRadius: 14,
        alignItems: "center",
        justifyContent: "space-around",
        padding: 10
    },
    content: {
        flexDirection: 'row',
        flex: 2
    },
    weatherContainer: {
        flex: 1
    },
    addressContainer: {
        flex: 1,
        justifyContent: 'center'
    },
    buttonContainer: {
        flex: 1
    }
});
export default Footer;
