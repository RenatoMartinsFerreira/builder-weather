import React from 'react';
import { StyleSheet, View} from 'react-native';
import { GenericTextComponent } from 'builderWeatherApp/src/components/presentation'

import colors from 'builderWeatherApp/src/commons/colors';

export const GenericCircularText = ({ text }) => (
  <View style={styles.container}  >
    <GenericTextComponent 
      text={text}
    />
  </View>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: colors.gold,
    borderRadius: 10000000,
    paddingHorizontal: 10,
    paddingVertical: 5,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  }
});
export default GenericCircularText;
