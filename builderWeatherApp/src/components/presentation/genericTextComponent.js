/* eslint-disable import/prefer-default-export */
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';
import colors from 'builderWeatherApp/src/commons/colors';
import { fontScale } from 'builderWeatherApp/src/commons/scaling';

const StyleguideItem = {
    HEADING: 'HEADING',
    DEFAULT: 'DEFAULT',
}

export const GenericTextComponent = ({
    // Reference to: https://app.zeplin.io/project/5c9a46bf85fb707808eaedc7/styleguide
    styleguideItem,
    text,
    color,
    opacity,
    textAlign,
    marginTop,
    marginBottom,
    numberOfLines,
    strike
}) => {
    let currentStyle;

    switch (styleguideItem) {
        case StyleguideItem.HEADING:
            currentStyle = styles.heading
            break 
        default:
            currentStyle = styles.default
            break
    }

    return (
        <Text style={[currentStyle, {
            opacity: opacity,
            color: color,
            textAlign: textAlign,
            marginTop: marginTop,
            marginBottom: marginBottom
        }, !!strike && styles.strike]}
            allowFontScaling={false}
            numberOfLines={numberOfLines}>{text}</Text>
    )
};

GenericTextComponent.defaultProps = {
    styleguideItem: StyleguideItem.DEFAULT,
    text: '',
    color: colors.black,
    opacity: 1,
    textAlign: 'left',
    marginTop: 0,
    marginBottom: 0,
    numberOfLines: 99999,
    strike: false
};

GenericTextComponent.propTypes = {
    styleguideItem: PropTypes.oneOf(Object.keys(StyleguideItem)),
    text: PropTypes.string.isRequired,
    opacity: PropTypes.number,
    textAlign: PropTypes.string,
    marginTop: PropTypes.number,
    marginBottom: PropTypes.number,
    numberOfLines: PropTypes.number,
    strike: PropTypes.bool
};

GenericTextComponent.StyleguideItem = StyleguideItem;

const styles = StyleSheet.create({
    default: {
        fontSize: fontScale(18),
        lineHeight: fontScale(24)
    },
    heading:{
        fontSize: fontScale(36),
    }
});
