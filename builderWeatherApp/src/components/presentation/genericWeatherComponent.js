import React from 'react';
import { StyleSheet, View } from 'react-native';
import { GenericTextComponent } from 'builderWeatherApp/src/components/presentation'
import PropTypes from 'prop-types';

import colors from 'builderWeatherApp/src/commons/colors';

export class GenericWeatherComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container} >

        <View style={styles.currentContainer} >
          <GenericTextComponent
            text={this.props.current + 'ºC'}
            styleguideItem={GenericTextComponent.StyleguideItem.HEADING}
          />
        </View>

        <View style={styles.bottomContent} >
          <GenericTextComponent
            text={this.props.max + 'ºC'}            
            color={colors.Gray53}
          />
          <GenericTextComponent
            text={this.props.min + 'ºC'}
            color={colors.Gray73}
          />
        </View>

      </View>
    );
  }
}

GenericWeatherComponent.defaultProps = {
  min: '0',
  max: '0',
  current: '0'
};

GenericWeatherComponent.propTypes = {
  min: PropTypes.string,
  max: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 20
  },
  currentContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});
export default GenericWeatherComponent;
