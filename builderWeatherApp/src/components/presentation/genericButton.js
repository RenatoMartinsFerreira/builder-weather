import React from 'react';
import { StyleSheet, View, TouchableOpacity, ScrollView, Text } from 'react-native';
import { verticalScale } from 'builderWeatherApp/src/commons/scaling';
import { GenericTextComponent } from 'builderWeatherApp/src/components/presentation'

import colors from 'builderWeatherApp/src/commons/colors';

export const GenericButton = ({ text, onPress }) => (
  <TouchableOpacity style={styles.container}
    onPress={onPress}
  >
    <GenericTextComponent
      text={text}
      color={colors.white}
    />
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.gold,
    borderRadius: 8,
    padding: 10,
    alignItems: 'center',
  }
});
export default GenericButton;
