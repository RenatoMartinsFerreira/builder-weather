import Store from "builderWeatherApp/src/redux/store";
import { WeatherService } from "builderWeatherApp/src/services";
import Geolocation from '@react-native-community/geolocation';
import {
  saveWeatherModelData,
} from "builderWeatherApp/src/redux/actions";
export class WeatherModel {
  static getWeather = (lat, lon) => {

    return new Promise((resolve, reject) => {
      const weather = new WeatherModel();
     
      weather.lat = lat
      weather.lon = lon

      weather.weatherService.getWeatherInfo( lat, lon ).then((response) => {
        weather.temp_max = response.main.temp_max
        weather.temp_min = response.main.temp_min
        weather.temp = response.main.temp
        weather.humidity = response.main.humidity
        weather.place = response.name
        weather.weather = response.weather.id
        weather.clouds = response.clouds.all
        weather.windSpeed = response.wind.speed
        this.saveWeather(weather)
        resolve(weather)
      }, (e) => { reject(e) })
    });
  };

  static saveWeather = (weatherData) => {
    Store.dispatch(saveWeatherModelData(weatherData));
  };

  getCurrentWeather() {
    return new Promise((resolve, reject) => {
      Geolocation.getCurrentPosition((info) => { WeatherModel.getWeather(info.coords.latitude, info.coords.longitude).then((weather) => { resolve (weather) }) });
    });
  }

  constructor() {
    const weatherData = Store.getState().weatherReducer;

    this.lon = weatherData.lon;
    this.lat = weatherData.lat;
    this.temp_min = weatherData.temp_min;
    this.temp_max = weatherData.temp_max;
    this.temp = weatherData.temp;
    this.weather = weatherData.weather;
    this.clouds = weatherData.clouds;
    this.windSpeed = weatherData.windSpeed;
    this.humidity = weatherData.humidity;
    this.place = weatherData.place;

    this.weatherService = new WeatherService() ;
  }

  cleanWeatherDS(action = null) {

  }

}
export default WeatherModel;

