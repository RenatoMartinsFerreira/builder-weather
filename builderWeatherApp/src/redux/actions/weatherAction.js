import {
    SAVE_WEATHER_MODEL_DATA,
} from './actionTypes';

export const saveWeatherModelData = payload => ({
    type: SAVE_WEATHER_MODEL_DATA,
    payload
});
