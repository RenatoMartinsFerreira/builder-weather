import {
  SAVE_WEATHER_MODEL_DATA,
} from '../actions/actionTypes';

const INITIAL_STATE = {
  finish: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SAVE_WEATHER_MODEL_DATA:
      return {
        finish: action.payload,
      };
    default:
      return { ...state };
  }
};
